import scrapy
import datetime
from urllib.parse import urljoin


class RapsberrySpider(scrapy.Spider):
    name = "rapsberry_spider"
    start_urls = ['https://www.canakit.com/raspberry-pi']

    def parse(self, response):
        self.domain_url = response.url.replace('/raspberry-pi', '')
        category_selector = 'ul.shadowBox li a.outerLink ::attr(href)'
        categories = response.css(category_selector).extract()

        for category in categories:
            url = urljoin(response.url, category)

            yield scrapy.Request(url, callback=self.parse_products_from_category)

    def parse_products_from_category(self, category):
        product_selector = 'ul.shadowBox'
        products = category.css(product_selector)

        for product in products:
            product_url_selector = '.outerLink ::attr(href)'
            product_image_url_selector = '.outerLink span img ::attr(src)'
            product_name = '.outerLink span ::text'
            product_price = '.productRegPrice ::text'

            yield {
                'product_name': product.css(product_name).extract_first(),
                'product_url': self.domain_url + (product.css(product_url_selector).extract_first()),
                'product_image_url': product.css(product_image_url_selector).extract_first(),
                'product_price': product.css(product_price).extract_first(),
                'time': datetime.datetime.now()
            }



